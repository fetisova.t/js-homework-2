/**
 * Created on 11.06.2019.
 */
/*Не обязательное задание продвинутой сложности:

 Проверить, что введенное значение является целым числом. Если данное условие не соблюдается, повторять вывод окна на экран до тех пор, пока не будет введено целое число.
 Считать два числа, m и n. Вывести в консоль все простые числа (http://ru.math.wikia.com/wiki/%D0%9F%D1%80%D0%BE%D1%81%D1%82%D0%BE%D0%B5_%D1%87%D0%B8%D1%81%D0%BB%D0%BE) в диапазоне от m до n (меньшее из введенных чисел будет m, бОльшее будет n). Если хотя бы одно из чисел не соблюдает условие валидации, указанное выше, вывести сообщение об ошибке, и спросить оба числа заново.*/

function checkNum2(userNum1, userNum2) {
    userNum1 = parseFloat(prompt('Enter FIRST integer number:  '));
    userNum2 = parseFloat(prompt('Enter SECOND integer number:  '));
    while (!Number.isInteger(userNum1) || !Number.isInteger(userNum2)) {
        userNum1 = parseFloat(prompt("You've entered NOT an integer number! Enter FIRST integer number:  ", userNum1));
        userNum2 = parseFloat(prompt("You've entered NOT an integer number! Enter SECOND integer number:  ", userNum2));
        userNum = parseFloat(prompt("You've entered NOT an integer number! Enter an integer number:  "));
    }
    if (userNum1 < userNum2) {

        for (let i=userNum1; i<=userNum2; i++){
            for(var j = 2; j<=i; j++){
                //var использован так как если использовать let то  в следующем if программа не видит переменной j, ругается что она не объявлена
                if (i%j === 0) break;
            }

            if(j===i) console.log(`Простое число в диапазоне от ${userNum1} до ${userNum2}: `, i);
        }
    }else{
        for (let i=userNum2; i<=userNum1; i++){
            for(var j = 2; j<=i; j++){
                if (i%j === 0) break;
            }

            if(j===i) console.log(`Простое число в диапазоне от ${userNum2} до ${userNum1}: `, i);
        }
    }
}



checkNum2();